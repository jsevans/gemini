--------------------------------------------------------------------------------

#'Learning CW' ##'2005-11-21T19:04:38+00:00' status: publish permalink: /2005/11/learning-cw-2  excerpt: '' type: post id: 7 category:     - Uncategorized tag:     - 'General Amateur Radio' post_format: [] restapi_import_id:     - 5a108276781c0     - 5a108276781c0 original_post_id:     - '7'     - '7'

--------------------------------------------------------------------------------

Last January, I passed the General Class written test with flying colors. I also failed the CW test miserably. In June, I failed it again, and I have one month to take it one last time or else I have to retake the written class test over again. I’m not looking forward to that at all. So, I’m putting my nose to the grindstone and I will pass the test next time.

My first learning method was visually. I was using a book called Morse Code for the Rest of Us[1] that had each letter in code written with cute little pictures. I learned the code that way, and I can even send code at a decend rate now, but I can’t hear it. I have trouble telling the dit’s from the dah’s. After a few seconds, I get lost and everything jumbles together. That’s the problem with learning CW visually, or so I’m told. You have to start learning code by ear. That way, your ears pick up the sounds and start recognizing the patterns immediately.

Now I am using K7QO’s Code Course[2]. The only problem I see now is with me. I have to make the time to do this. I wish I had more patience for this kind of thing. I know that once I can hear the code and translate it instantly, I will never loose it and I will be up to 20WPM in no time. It’s getting there that will be the problem.



=> http://www.users.qwest.net/%7Edanhayman/ 1: http://www.users.qwest.net/%7Edanhayman/
=> http://www.k7qo.net/ 2: http://www.k7qo.net/