--------------------------------------------------------------------------------

#e17 ##'2006-08-09T15:17:33+00:00' status: publish permalink: /2006/08/e17  excerpt: '' type: post id: 126 category:     - Uncategorized tag:     - Linux post_format: [] restapi_import_id:     - 5a108276781c0     - 5a108276781c0 original_post_id:     - '32'     - '32'

--------------------------------------------------------------------------------

Is e17[1] the killer app that Linux has been waiting for?

I downloaded the Ubuntu binaries for e17 last night, and I was blown away. My computer’s specs are modest by most geek-machine comparisons, but the eye candy was brilliant. The speed of the desktop was very good and everything was nicely arranged by default to give a pleasent view of this desktop manager. Mac snobs like to gripe about how easy everything is to use and how well everything is laid out. With the slight lurning curve of understanding how Linux packages work as opposed to the hunt, download, and install philosphy of Mac and Windows users, I think a well thoughtout Linux based on a clean e17 desktop could be the bridging point for those on the fence between the Mac’s and PC’s. A Linux box will be, by design, will be cheaper than a Mac and runs on the same hardware as Windows, but has the same ease of use as a Mac. In my opinion, the biggest problem with Linux going mainstream is native hardware support and 3rd Party software.

A live cd containing e17 can be found here[2]. Download it, burn it, and enjoy it.

Technorati Tags: linux[3], e17[4], enlightenment[5]



=> http://enlightenment.org/ 1: http://enlightenment.org/
=> http://elivecd.org/ 2: http://elivecd.org/
=> http://technorati.com/tag/linux 3: http://technorati.com/tag/linux
=> http://technorati.com/tag/e17 4: http://technorati.com/tag/e17
=> http://technorati.com/tag/enlightenment 5: http://technorati.com/tag/enlightenment