--------------------------------------------------------------------------------

#'Create a complete Tor Onion Service with Docker and OpenSUSE (EXPANDED)' ##'2018-05-27T15:07:45+00:00' status: publish permalink: /2018/05/create-a-complete-tor-onion-service-with-docker-and-opensuse-expanded  excerpt: '' type: post id: 904 category:     - Uncategorized tag: [] post_format: [] timeline_notification:     - '1527426465'     - '1527426465' publicize_twitter_user:     - penguinpanicked     - penguinpanicked publicize_linkedin_url:     - 'https://www.linkedin.com/updates?discuss=&amp;scope=9165599&amp;stype=M&amp;topic=6406490946527129600&amp;type=U&amp;a=p8We[1]'     - 'https://www.linkedin.com/updates?discuss=&amp;scope=9165599&amp;stype=M&amp;topic=6406490946527129600&amp;type=U&amp;a=p8We[2]'

--------------------------------------------------------------------------------

```html
<iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="360" loading="lazy" src="https://www.youtube.com/embed/iUxiTk6w1sc?start=1076&feature=oembed" title="openSUSE Conference 2018 - Create a complete Tor .onion site with Docker and OpenSUSE in less than 1" width="640"></iframe>
```

I wrote this presentation on the weekends in April and May and it’s didn’t have quite the details that I wanted to put into it. Mostly I wanted it to be short and engaging. Putting in every detail that I wanted would have (I thought) been long and boring. I would like to take the time here to expand what went into the presentation and to make it a little more interesting.


=> ../../../../uploads/2018/05/htw1-1.png htw1 (1) [IMG]

I don’t really like these diagrams. They already existed on tor-project.org and were made by the EFF but it’s too high level and they don’t really tell the story that I wanted to tell. The actual description on how onion services with is here: https://www.torproject.org/docs/onion-services.html.en[3]

I could have done a better job than what I did. The way that I prefer to describe it is like this:

*After configuring your /etc/tor/torrc file, you run systemctl tor start and the local Tor daemon reaches out to the Tor networks and lets it know that you are running a local onion service. This creates a two-way link between your machine and the Tor network which is UDP traffic rather than TCP/IP as the Tor network never sees your actual local IP.*

If I’m honest with myself, this is still pretty weak but it’s better than what I had. The best thing would be to take the information from Tor website almost verbatim and made slides but I didn’t do that.


=> ../../../../uploads/2018/05/screenshot_20180527_142714.png Screenshot_20180527_142714 [IMG]

I glossed over this when I should have made more slides to help fill out the presentation. The brief anecdotes and really touch on a lot of reason why I think people should be using onion services such as:


=> ../../../../uploads/2018/05/screenshot_20180527_143138.png Screenshot_20180527_143138 [IMG]

Being in the spotlight might just mean having a job where people know who you are. No matter who you are, there is a good chance that your political ideas will offend someone. Personally, I keep a strict no religion/no politics policy for myself at work. I just nod my head to everyone like I agree and/or understand. At home, it’s a different story. I am a political person and I care deeply about politics but I don’t want that interfering with my role at my company and I’m not alone. This is the point that would have had more punch than what I made and would have been a better case study on why onion sites are useful and needed.


=> ../../../../uploads/2018/05/screenshot_20180527_1440361.png Screenshot_20180527_144036 [IMG]

The first two should have been one topic and the last should have been a call back to a better description of how the onion routing and encryption works.


=> ../../../../uploads/2018/05/silk_road_marketplace_item_screen.jpg Silk_Road_Marketplace_Item_Screen [IMG]

Nefarious websites such as Silk Road[4] and the Playpen[5] were better case studies on how onion services are misused.

Finally, I think more details on how the docker-compose files are built would have been more useful as well as some hands-on interaction. Those file are all at my Github[6] but I ran through them so quickly I didn’t really give the audience time to see them during the presentation.



=> https://www.linkedin.com/updates?discuss=&amp;scope=9165599&amp;stype=M&amp;topic=6406490946527129600&amp;type=U&amp;a=p8We 1: https://www.linkedin.com/updates?discuss=&amp;scope=9165599&amp;stype=M&amp;topic=6406490946527129600&amp;type=U&amp;a=p8We
=> https://www.linkedin.com/updates?discuss=&amp;scope=9165599&amp;stype=M&amp;topic=6406490946527129600&amp;type=U&amp;a=p8We 2: https://www.linkedin.com/updates?discuss=&amp;scope=9165599&amp;stype=M&amp;topic=6406490946527129600&amp;type=U&amp;a=p8We
=> https://www.torproject.org/docs/onion-services.html.en 3: https://www.torproject.org/docs/onion-services.html.en
=> https://en.wikipedia.org/wiki/Silk_Road_(marketplace) 4: https://en.wikipedia.org/wiki/Silk_Road_(marketplace)
=> https://en.wikipedia.org/wiki/Playpen_(website) 5: https://en.wikipedia.org/wiki/Playpen_(website)
=> https://github.com/tgeek77 6: https://github.com/tgeek77