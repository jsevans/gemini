--------------------------------------------------------------------------------

#'I See Pretty Colors!' ##'2005-07-12T15:49:02+00:00' status: publish permalink: /2005/07/i-see-pretty-colors  excerpt: '' type: post id: 119 category:     - Uncategorized tag:     - Linux post_format: [] restapi_import_id:     - 5a108276781c0     - 5a108276781c0 original_post_id:     - '28'     - '28'

--------------------------------------------------------------------------------

Windows Longhorn 5203 Screenshots[1]

Here are a few free screenshots of the new Windows version that is due out next year. It will be called, “Longhorn”.

I see a lot of “eye candy” here i.e. a lot of fancy graphics. The problem is that it will require equally fancy hardware to support it, not to mention the cost of the OS itself.

This is Fedora Core 4:
http://shots.osdir.com/slideshows/363%5C_or/1.png[2][3]
http://shots.osdir.com/slideshows/362%5C_or/1.png[4][5]

This is Knoppix 3.9
http://www.softpedia.com/screenshots//Knoppix-Live-GNULinux%5C_2.jpg[6][7]

This is Mandrake Linux using Super Karamba
http://meowing.ccm.uc.edu/%7Eico/Linux/Borealis/Borealis%5C_manual/2.0/desktop1.jpg[8][9]

This is an unknown distro running the Enlightenment Window Manager
http://xwinman.org/screenshots/enl-jackson.jpg[10]

Both Linux and Windows offers some pretty impressive eye candy, but two things stand out.

1. In Linux, the desktop is so customizable, that you won’t believe it. Everything is changeable, and you don’t even need to be a programmer to do it.

2. You can have an impressive looking (and functional!) desktop with very modest hardware requirements under Linux. If you have anything less than a 1Ghz PC, I would bet that you may have to upgrade your PC in order to run the newest version of Windows. This isn’t necessarily the case with Linux.



=> http://www.flexbeta.net/main/comments.php?catid=1&amp;shownews=13839 1: http://www.flexbeta.net/main/comments.php?catid=1&amp;shownews=13839
=> http://shots.osdir.com/slideshows/363%5C_or/1.png 2: http://shots.osdir.com/slideshows/363%5C_or/1.png
=> http://shots.osdir.com/slideshows/363_or/1.png 3: http://shots.osdir.com/slideshows/363_or/1.png
=> http://shots.osdir.com/slideshows/362%5C_or/1.png 4: http://shots.osdir.com/slideshows/362%5C_or/1.png
=> http://shots.osdir.com/slideshows/362_or/1.png 5: http://shots.osdir.com/slideshows/362_or/1.png
=> http://www.softpedia.com/screenshots//Knoppix-Live-GNULinux%5C_2.jpg 6: http://www.softpedia.com/screenshots//Knoppix-Live-GNULinux%5C_2.jpg
=> http://www.softpedia.com/screenshots//Knoppix-Live-GNULinux_2.jpg 7: http://www.softpedia.com/screenshots//Knoppix-Live-GNULinux_2.jpg
=> http://meowing.ccm.uc.edu/%7Eico/Linux/Borealis/Borealis%5C_manual/2.0/desktop1.jpg 8: http://meowing.ccm.uc.edu/%7Eico/Linux/Borealis/Borealis%5C_manual/2.0/desktop1.jpg
=> http://meowing.ccm.uc.edu/%7Eico/Linux/Borealis/Borealis_manual/2.0/desktop1.jpg 9: http://meowing.ccm.uc.edu/%7Eico/Linux/Borealis/Borealis_manual/2.0/desktop1.jpg
=> http://xwinman.org/screenshots/enl-jackson.jpg 10: http://xwinman.org/screenshots/enl-jackson.jpg