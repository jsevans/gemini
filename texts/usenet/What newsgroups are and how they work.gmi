# Subject: What newsgroups are and how they work
## Date: Sat, 26 Apr 2014 10:32:37 -0900

Path: not-for-mail
From: nnq-board@anta.net (NNQ Moderation Team)
Newsgroups: news.announce.newusers,news.newusers.questions
Subject: What newsgroups are and how they work
Followup-To: news.newusers.questions
Date: Sat, 26 Apr 2014 10:32:37 -0900
Lines: 88
Approved: NNQ Moderation Team <nnq-board@anta.net>
Message-ID: <1lkex1n.14shmzc5oj6seN%nnq-board@anta.net>
X-Trace: individual.net 7Mfzyske9MsH71CtRrFm+gEIACbtStwiLVfxn/RmsgxkRgbHEb
X-Orig-Path: nnq-board
Cancel-Lock: sha1:ElzH9RD4acfj9K6HkDnW/cDcuZ4=
User-Agent: MacSOUP/2.8.3 (Mac OS X version 10.7.5 (x86))
X-Received-Bytes: 4891
X-Received-Body-CRC: 862471780

*Newsgroups* are a means of public discussion and distribution of
material to a large number of people. They share this fundamental
purpose with electronic mailing lists, Web-based bulletin boards,
etc. Newsgroups can appear to be very much like one of these
other kinds of forums, depending on how you access them.
Nevertheless, newsgroups are different from them in important
ways, and each kind of forum has its own quirks, advantages and
disadvantages.

Newsgroup messages are not stored in a single central location (as
with a Web-based bulletin board) or distributed from a single
central location (as with an electronic mailing list). Instead,
they are stored on a multitude of *news servers* that are operated
by Internet service providers (ISPs) for their customers, by
schools and universities for their students and staff, by
companies for their employees, by commercial and free News Service
Providers (NSPs), etc.

When someone posts a message in a newsgroup, it is first stored on
his/her provider's news server. That server then distributes
copies of the message to its *peers*, that is, to other servers
with which it has agreed to exchange newsgroup messages directly.
Those servers then distribute copies to *their* peers, and so on,
until (in principle) all the servers which carry that newsgroup
have a copy of the message. When someone reads a message in a
newsgroup, he/she is reading the copy that is stored on his/her
provider's news server.

Some newsgroups are *moderated*. In these newsgroups, the
poster's server sends messages directly to a *moderator* for
inspection. If the moderator approves a message, he/she posts it
on his/her provider's news server, and it propagates from there to
other servers (including yours).

For more details about the newsgroup distribution mechanism, see:

   <http://www.anta.net/misc/nnq/how-it-works.html>.

People read and post to newsgroups using *news clients* (also
called *newsreaders*) such as Mozilla Thunderbird, and Microsoft
Outlook Express, and a variety of other software packages for
Windows, Unix, Linux, MacOS and other operating systems. These
newsreaders communicate with news servers via the NNTP protocol
(Network News Transfer Protocol). NNTP also specifies how news
servers exchange messages with each other.

Some servers make newsgroup messages available via a Web-based
interface that can be used with any Web browser, for example
<http://groups.google.com/>.

If you are reading this article in news.announce.newusers, please
note that this group is strictly moderated and does not accept
ordinary postings. If you try to post a follow-up (response) to
one of these articles, it will automatically appear only in
news.newusers.questions. If you try to post a new message (that
is not a follow-up) in news.announce.newusers, it will
automatically be rejected.

If you want to post questions or comments about this article,
please post them in news.newusers.questions instead.

Also, please send e-mail to us only if you have specific
corrections to the material in this posting, or other
administrative questions about news.announce.newusers. For
answers to other questions, please use the following resources:

 + Your newsreader software's documentation
 + Your Internet service provider's support staff
 + The news.newusers.questions web site
      <http://www.anta.net/misc/nnq/>
 + A web search engine such as <http://www.google.com/> or
      <http://search.yahoo.com/>
 + The newsgroup news.newusers.questions, for questions about
      newsgroups
 + The newsgroups news.software.readers for specific help with
   newsreaders or for newsreader recommendations
 + Other appropriate newsgroups, for other kinds of questions

 + One source of information about various newsreaders and news
   service providers can be found at <http://www.newsreaders.com>

If you would like to post a periodic FAQ to news.announce.newusers,
please email the moderators at the address below.

This article was last revised on 11 February 2011.

--
The NNQ Moderation Board <nnq-board@anta.net>
