# Subject: Common questions about using newsgroups
## Subject: Common questions about using newsgroups

Path: eternal-september.org!reader01.eternal-september.org!reader02.eternal-september.org!news.eternal-september.org!news.eternal-september.org!news.eternal-september.org!feeder.eternal-september.org!feeder.erje.net!eu.feeder.erje.net!fu-berlin.de!uni-berlin.de!individual.net!not-for-mail
From: nnq-board@anta.net (NNQ Moderation Team)
Newsgroups: news.announce.newusers,news.newusers.questions
Subject: Common questions about using newsgroups
Followup-To: news.newusers.questions
Date: Wed, 19 Mar 2014 09:19:37 -0900
Lines: 270
Approved: The NNQ Moderation Board <nnq-board@anta.net>
Message-ID: <1lipzkk.1a2a57q16us2awN%nnq-board@anta.net>
X-Trace: individual.net F9Yfd69yVGuIKwsoubj8EQGovBeGI9B4ApGigBMgKJbbv/Ou/w
X-Orig-Path: nnq-board
Cancel-Lock: sha1:/XReiXlSJrUKT6WoDjf+zClTSto=
User-Agent: MacSOUP/2.8.3 (Mac OS X version 10.7.5 (x86))
Xref: news.eternal-september.org news.announce.newusers:44 news.newusers.questions:217

Introduction
------------

Following are some questions that newcomers to newsgroups often
ask and answers to them. It will help if you have some idea of
how newsgroups work in general, as described in the article "What
newsgroups are and how they work."

If you are reading this article in news.announce.newusers, please
note that this group is strictly moderated and does not accept
ordinary postings. If you try to post a follow-up (response) to
one of these articles, it will automatically appear only in
news.newusers.questions. If you try to post a new message (that
is not a follow-up) in news.announce.newusers, it will
automatically be rejected.

If you want to post questions or comments about this article,
please post them in news.newusers.questions instead.

Also, please send e-mail to us only if you have specific
corrections to the material in this posting, or other
administrative questions about news.announce.newusers. For
answers to other questions, please use the following resources:

 + Your newsreader software's documentation
 + Your Internet service provider's support staff
 + The news.newusers.questions Web site
      <http://www.anta.net/misc/nnq/>
 + A web search engine such as <http://www.google.com/> or
      <http://search.yahoo.com/>
 + The newsgroup news.newusers.questions, for questions about
      newsgroups
 + Other appropriate newsgroups, for other kinds of questions

This article was last revised on 31 December 2009.

If you would like to post a periodic FAQ to news.announce.newusers,
please email the moderators at the address below for guidelines and
instructions.

Contents
--------
 Q1: How do I shut off all these messages that are filling up my
mailbox?
 Q2: I posted a message [or read a certain message] a little while
ago and now I can't find it again. Where did it go?
 Q3: I posted a message a few hours ago, and it still hasn't
appeared. What happened to it?
 Q4: How long does it take to get a response to a newsgroup message?
 Q5: Where should I look for a response to a question that I posted?
 Q6: Should I post or e-mail a response to someone else's posting?
 Q7: How do I delete a message that I posted?
 Q8: Why do some people put "nospam" or something similar in their
return addresses when posting?
 Q9: How do I go to other newsgroups, or find ones that interest me?
 Q10: How do I create my own newsgroup?

-----

Q1: Help! How do I shut off all these messages that are filling
up my mailbox?

A1: Unlike electronic mailing-list messages, newsgroup messages
are *not* dumped into your mailbox or other personal storage
space. They are stored on your ISP's (or school's or company's)
news server, in a sort of database that all the server's users can
access. Most commonly-used newsreading software (*newsreader* for
short) downloads (by default) only those specific individual
messages that you choose to read, and does not save them on your
disk unless you specifically tell it to do so.

Many newsreaders do have an _offline_mode_ which allows you to
download all recently-arrived messages at once, so you can then
select and read messages without staying connected to the
Internet. But you normally have to configure your software to do
this specifically.

-----

Q2: I posted a message [or read a certain message] a little while
ago and now I can't find it again. Where did it go?

A2: Most newsreaders automatically hide messages that you've
already read, on the assumption that you don't want to read the
same messages over and over again. There should be a command
(perhaps called "Unmark" or "Show All Messages") which makes them
visible again.

-----

Q3: I posted a message a few hours ago, and it still hasn't
appeared. What happened to it?

A3: Assuming that your news server is working properly, it may be
that the newsgroup that you posted to is *moderated*. When you
post a message to a moderated group, your news server e-mails it
to the moderator for inspection, instead of posting it
immediately. It can take a while before the moderator gets around
to inspecting your message and deciding whether to post it. If he
does post it (on his own news server), it can take a while for it
to propagate back to your server. If he rejects it, he usually
tries to e-mail it back to you with an explanation, but if your
return address is incorrect, you won't get it.

If the newsgroup is not moderated, your newsreader might simply be
hiding your message from you. Look for a command called "Show All
Messages" or "Unmark All" or something similar.

-----

Q4: How long does it take to get a response to a newsgroup message?

A4: It depends. Newsgroups are not a synchronous service such as
ICQ, chat rooms or AOL Instant Messenger, in which participants
communicate directly with each other in real time. Therefore, you
must not expect an immediate response to one of your messages.

First, it takes a certain amount of time for your message to
propagate to other news servers. If your server is
well-connected, your message will probably appear on many other
servers within an hour or so. But some servers can take a day or
more to receive your postings, especially if they (or some
intermediate servers) are having problems. Then it takes a while
for people to find your message and read it. Most people probably
check a newsgroup once a day or less often. If you have an
obscure or specialized question it may be several days before
someone who knows the answer sees it. Finally, it takes more time
for the response, if any, to propagate back to your own server so
you can read it.

If you haven't received a response after a few days, it might be
appropriate to post a followup message, with more details, or with
a more descriptive subject line.

-----

Q5: Where should I look for a response to a question that I posted?

A5: Normally, you should expect answers to be posted to the
newsgroup, if they might be useful to other people. Some
newsreaders can automatically select responses to your postings.
Otherwise, you should remember the subject lines of your postings,
and look for them for responses. You should expect a private
e-mail response only if it is of a personal nature. Some people
will e-mail you regardless, or send you "courtesy" e-mail copies
of posted responses, but you shouldn't count on this.

You should specifically request e-mail responses only if you have
a good reason. Saying something like "Please e-mail me because I
don't read this group often" will make many people angry at you.
The usual attitude is, "if you expect us to take the time to
answer your questions, you need to take the time to watch for the
answers." If you do have a good reason for wanting e-mail
responses, state it. For example, if your news server loses many
messages, you might miss a posted response, and it is acceptable
to ask for an e-mail _copy_ of a posted response.

-----

Q6: Should I post or e-mail a response to someone else's posting?

A6: In keeping with the answer to the previous question, you
should post the response if the information might be useful to
other people besides the person that you're responding to. If
your response is personal in nature, and of no interest to anyone
else, then you should e-mail it.

However, you should be aware that many active posters do not want
e-mail responses. They prefer to keep all discussion in the
newsgroup and some may actually get angry at you if you send them
e-mail. If in doubt, don't.

Most newsreaders give you the option of posting the response, with
an e-mail copy to the original posting's author. If you do this,
you should warn the recipient clearly about this in the body of
your message, so that he/she can decide how best to respond in
turn, if necessary.

-----

Q7: How do I delete a message that I posted?

A7: Practically speaking, you can't.

Most newsreaders have a "Cancel" command. This posts a special
kind of message (a *control* message) that asks news servers to
delete a specified message. In theory, a message can be canceled
only by the author, or by his news server administrator).
However, it is rather easy to forge cancels in another person's
name, and the resulting abuse has caused most news server
administrators to disable cancels on their servers. Therefore,
you cannot rely on a cancel to remove a message that you posted
from all news servers.

However, normally you don't need to delete your own messages
anyway, because almost all news servers automatically *expire*
(remove) old messages to make room for new ones. They do this
either by removing messages that are older than a certain number
of days (which can vary from one newsgroup to another), or by
removing the oldest messages until there is enough space for the
new ones. Expiration policies vary from one server to another, so
your messages will still be visible on some servers after they
have expired on others.

You should be aware that some sites, e.g.
<http://groups.google.com/>, *archive* messages in some or many
newsgroups, more or less permanently, and provide some way to
search them easily. Therefore, it's not a good idea to post something
that could embarrass you with your spouse or employer or some other
significant person.

-----

Q8: Why do some people put "nospam" or something similar in their
return addresses when posting?

A8: It is an unfortunate fact of life that some people extract
e-mail addresses from newsgroup messages to build lists of
addresses for *spam* (unsolicited e-mail advertisements, etc.).
To counteract this, many people *munge* (modify) their e-mail
addresses in newsgroup messages, and usually give instructions on
how to reconstruct the actual address. Another remedy is to use a
separate address at a free e-mail provider such as
<http://mail.yahoo.com/> or <http://www.hotmail.com/>, only for
posting newsgroup messages.

This means that if you send an e-mail response to a newsgroup
message, you need to beware that you might need to *un-munge* the
author's e-mail address, or substitute another address, according
to instructions in the body of the message.

Also, if you do munge your address, please be sure to append ".invalid"
to the end of the address. For example, if your address is
<username@example.com> and you want to munge it by adding "NOSPAM" in
the address, it would become <usernameNOSPAM@example.com.invalid>.

-----

Q9: How do I go to other newsgroups, or find ones that interest me?

A9: How you *subscribe* to a newsgroup, that is, tell your
newsreader software which newsgroups you want to read, depends on
the particular software that you are using. Therefore, you should
_first_ search your menus, online help, or other documentation for
the word "subscribe". If that fails, ask in a newsgroup that
discusses your software, or in news.newusers.questions; if you ask
in n.n.q, please say which software you are using.

To search for newsgroups by subject, try the search features at
Google Groups <http://groups.google.com/>.

-----

Q10: How do I create my own newsgroup?

A10: Creating a widely-available newsgroup requires the
cooperation of thousands of news server administrators. They
usually want some evidence that the newsgroup will actually be
used, which usually means some kind of vote or interest poll, or a
request from one of their own customers. In general, creating an
effective newsgroup is not something to undertake lightly.

The exact procedure depends on what kind of group you want to
create. For more detailed information, see the
news.newusers.questions Web site:

   <http://www.anta.net/misc/nnq/ncreate.html>

--
The NNQ Moderation Board <nnq-board@anta.net>
