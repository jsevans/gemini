
 -------------------------------------------------------------------------
|                        TTTTT  H  H  EEEE                                |
|                          T    H  H  E                                   |
|                          T    HHHH  EEE                                 |
|                          T    H  H  E                                   |
|                          T    H  H  EEEE                                |
|                                                                         |
|        A       M      M       A    TTTTTTT  EEEEE   U   U  RRRR         |
|       A A     M M    M M     A A      T     E       U   U  R   R        |
|      A   A   M   M  M   M   A   A     T     EEE     U   U  RRRR         |
|      AAAAA   M    MM    M   AAAAA     T     E       U   U  R   R        |
|      A   A   M          M   A   A     T     EEEEE    UUU   R    R       |
|                                                                         |
|   CCCC   OO    MM MM   PPP   U  U  TTTTT  EEEE  RRRR   III  SSS  TTTTT  |
|  C      O  O  M  M  M  P  P  U  U    T    E     R   R   I   S      T    |
|  C      O  O  M  M  M  PPPP  U  U    T    EEE   RRRR    I    S     T    |
|  C      O  O  M     M  P     U  U    T    E     R   R   I     S    T    |
|   CCCC   OO   M     M  P      UU     T    EEEE  R   R  III  SSS    T    |
|-------------------------------------------------------------------------|
|  February 11, 1988                                      volume 1 no 1   |
 -------------------------------------------------------------------------
Table of Contents

Introduction
Dawn of a New Era
Dedication
World of Telecommunications
Future Belongs to Programmers
Try This (IBM)
Commodore Tips and Tricks
Why Learning Programming

INTRODUCTION

     This newsletter is to inform people of developments in an
effort to advance computer education. Workers at the Ford Rouge
Plant in Dearborn, MI. were denied computer programming classes.
There was an effort by administrators of the UAW-Ford program at
the Dearborn Engine Plant to kill interest in computers and
computer programming. We want to keep interest alive because
computers are the future. We want to disperse information to
users about computers. Since the computer is still in the early
stage of development, the ideas and experiences of the users need
to be shared and built on if this technology is to advance. To
this end, this newsletter is dedicated to all people interested
in learning about computers. We welcome articles, programs,
reviews, etc. We want this newsletter to help people use their
computers in ways that will be useful and fun.

DAWN OF A NEW ERA

   From the Age of Darkness to the Age of Enlightenment -- from
the Machine Age to the Mind Age, here we are. Let not any force or
forces keep it under wraps. Let it be free to cir culate in the
Public Domain. Let us base it upon principle, not on price, like
Truth or Love. From the Great Wall to the Great Pyramid, from the
hieroglyphics to the screen of the computer, mankind is still
progressing. So make the new born science, that has given us the
computer for the amateur and not as a prerogative of the pro-
fessional to be shrouded in secrecy from humanity, the choice of
the individual, not an election of a minority. From the falling
star to the falling apple, from the minute to the multitudinous,
from secrets to disclosure, I am pleased to endorse the amateur
method. Therefore I implore all to plan and to participate even
though I have been on disability for 26 years and have not had
the opportunity to participate in the great sea of knowledge that
has flowed over the Dam of Secrecy since I was inactivated
physically and mentally -- in my advanced years and state of
general debility I still see the mind of man the greatest
computer of all -- So Let Us Continue to Make Use of It to the
Advantage of the Masses - Come, Let Us Reason Together. With an
open mind and a free spirit, let me reiterate, there is so much
more to know, that what we do know, is still insignificant. It
gives me great pleasure to endorse this free-for-all program of a
restless mind.
      Floyd Hoke-Miller, UAW Retiree and Flint Sit Down Striker

DEDICATION

     This first issue of the "Amateur Computerist" is being
published on February 11, 1988. This date was chosen so that this
issue could be dedicated to the Flint Sit Down pioneers on the
victory of their battle to win industrial unionism 51 years ago.

   Floyd Hoke-Miller, whose article "Dawn of a New Era" appears
else where in this newsletter, was a sitdowner in Plant 4 in
Flint, MI during the Great Flint Sit-Down Strike. He continues
to participate in the battle for industrial unionism and for the
progress that industrial unionism has brought to this land.

   Another pioneer of the Flint Sit-Down Strike, Jack Palmer,
when he retired, wrote an article in his union newspaper in which
he tried to sum up the gains and unresolved problems that the
sitdowners had left behind them. He wrote, "Each generation has
to solve its own problems. The sit-down generation solved the
problem of organization. The postwar generation solved the
problem of pensions and inflation. Not entirely, but a good
start was begun. The present generation is faced with the
greatest problems of all. They are Automation, Peace and
Politics." (from "The Searchlight" (newspaper of UAW Local 659,
Flint, MI), April 21, 1960, p 2).

   The "Amateur Computerist" is an effort to encourage discussion
on the problem of Automation. Microcomputers are now an
important fact of life. They are new. The first microcomputer
design was announced to the public only 14 years ago. (It was the
Mark-8 by Jonathan Titus featured on the cover of the July,1974
issue of Radio Electronics.) Today, personal computers are
everywhere. They are affecting and changing homes, factories,
offices, etc. They are revolutionizing all fields of knowledge.
Therefore, it is crucial that computers not be kept from people -
that knowledge about computers be available to amateurs as well
as professionals.

   In a book written shortly before the invention of the personal
computer, Ted Nelson warns against allowing a computer
priesthood to develop. He writes,"Knowledge is power and so it
tends to be hoarded. Experts in any field rarely want people to
understand what they do, and generally enjoy putting people down.

   "Thus if we say that the use of computers is dominated by a
priesthood, people who splatter you with unintelligible answers
and seem unwilling to give you straight answers, it is not that
they are different in this respect from any other profession.
Doctors, lawyers and construction engineers are the same way."

   "But," he goes on, "computers are very special, and we have to
deal with them everywhere and this effectively gives the computer
priesthood a stranglehold on the operation of all large
organizations, of government bureaux, and anything else that they
run...."

   "It is imperative," he concludes, "for many reasons that the
appalling gap between public and computer insider be closed. As
the saying goes, war is too important to be left to the
generals....Guardianship of the computer can no longer be left to
a priesthood....Indeed, probably any group of insiders would have
hoarded computers just as much....But things have gone too far.
People have legitimate complaints about the way computers are
used, and legitimate ideas for ways they should be used which
should no longer be shunted aside." (from Computer Lib, pg 1-2.)

   Thus to deal with the problem of automation, it is necessary
for people to be familiar with computers, to use them, and to
know their capabilities and limitations. To that end, this news-
letter is dedicated to continuing the work begun by the Flint
Sit-Down Pioneers.


THE WORLD OF TELECOMMUNICATIONS

   Do you want advice about which programming language is worth
learning? Are you interested in a discussion on why the shuttle
blew up? These and many more questions were recently discussed on
a computer bulletin board system (bbs). Bbs's are part of the
world of telecommunications.

   For example, there is an on-line computer magazine on the BBS
"Chess Board." In an article on "Telecommunications: The
Interactive Process," the writer explains: A bulletin board sys-
tem (BBS) is a privately owned and maintained computer based
communications system. A person, or sometimes a group of people,
have invested computer, data storage, telephone lines and bills
and much time into giving others a means for communicating with
another with their computers. They each have their own reasons
and goals for investing hundreds or thousands of dollars and
hours to this activity."

   Another article on "Chess Board" points out that each bulletin
board has a purpose. It can be a fun board, with puzzles and
games, it can be a board whose purpose is an exchange of ideas
with discussion formats which include debates on various issues
like current events, world affairs, etc. It can be a board that
will let users exchange software through uploading and
downloading programs.

   Following are listed just a few of the many BBS's in the
Detroit area - (or if the number has a 1 before it, in this case,
it is because it is in the Ann Arbor area.)

    Genesis II (291-2520)  Has lots of files to download.
    PC Playhouse (381-8633)  Mostly IBM compatible information.
    Starship Enterprise (843-1581)  Can ask for prices on computer
        equipment.
    Chessie's BBS (291-2160)  Good discussion.
    Trading Post (882-7104)  In general good board.
    The Outpost (277-1513)  Good for downloading programs.
    General Store (728-2863)  Good for downloading.
    M-Net (1-994-6333)  Lots of lively, informative discussions,
    and can have on line conversations.

   If you want to call Ann Arbor, you can call thru MERIT so it
isn't a toll call. An article on using merit will follow in a fu-
ture issue if there is interest.

   Along with BBS's in one's local area which are usually
available free of charge, there are also commercial services like
Compuserve or the Source. They bill users a fee for the time on
line.

   Another user sums up the value of telecommunications: "You
exchange ideas, you discuss, you might not see these people, but
you have connection with them thru the modem so if you're stuck
at home, you're still out in the world. You exchange ideas,
messages programs, etc. You reach a whole community."

TRY THIS

This is a graphics program for IBM PC & compatible machines.

5  REM Trythis.bas
10 KEY OFF:CLS:SCREEN 1
20 X=4*RND:Y=4*RND:IF X=Y THEN 20
30 COLOR X,Y
40 FOR A=-120 TO 120 STEP 4
50 FOR B=0 TO 1
60 LINE (160,100)-(A,199*B),RND*4
70 LINE (160,100)-(319-A,199*B)
80 NEXT B
90 NEXT A
100 FOR D= 1 TO 5
110 FOR C=1 TO 23
120 COLOR 4*RND,4*RND
130 CIRCLE (160,100),5*C,4*RND
140 NEXT
150 NEXT
160 FOR E=1 TO 10
170 PAINT(160,100-E*2.5+2),RND*4
180 NEXT
190 LOCATE 13,17:PRINT"THE END"


THE FUTURE BELONGS TO PROGRAMMERS

   An article in the Jan/Feb. 1988 issue of Computer Update, the
magazine of the Boston Computer Society, explained that Microsoft
is recommending that computer users learn to program in
B.A.S.I.C. Microsoft sponsored a two day seminar in the state of
Washington in October, 1987 for representatives of big computer
clubs. Microsoft was expected to introduce some of its new
products. Instead, to the amazement of many, Microsoft used the
seminar to explain the importance of learning to program.
"Microsoft today is bustling with activity...Oddly enough,
Microsoft chose not to talk about any of these activities with
the user group community. Instead, it focused all of the sessions
on its work on programming languages," reported the Boston
Computer Society representative.

   The article goes on to explain, "Microsoft believes the future
belongs to programmers. Although programming languages were once
thought to be relics of the early days of personal computers,
they are enjoying tremendous growth today. As users become more
sophisticated, Microsoft believes, they will eventually find
themselves needing performance and specialized functions that
only a programming language can provide."

   The article quotes a Microsoft engineer, "In the future,
everything should have programmability." The User Group
Representatives were further surprised by another development at
the conference. Not only did Microsoft stress the importance of
programming, they also stressed the importance of the B.A.S.I.C.
programming language. The writer noted, "Most serious programmers
consider BASIC an obsolete language." The writer went on to cite
the slowness and lack of sophistication of BASIC as the reason.
"More and more" programmers "are opting for C as their language
of choice," he explains.

   But not only is BASIC available on more personal computers
than any other language, it is also easier to learn than any
other language. "For this reason," the article explains,
"Microsoft sees BASIC as "the language of programmability for the
future."

   Bill Gates, Chairman of Microsoft, is quoted as recognizing
"the need for a `universal macro language' for personal
computers." The article goes on to suggest that "This language
would allow users to write procedures that work on all different
application programs and operating systems. (It could, for
example, permit you to write a macro in 1-2-3 that called up a
program in dBase III and then transferred information to
Pagemaker.)"

   The article sums up the conference, "Although the presenters
did not say so directly, they implied that Microsoft was working
to make BASIC the basis of this universal micro language." The
result would be that BASIC, "then could become the Esperanto of
the applications software world."

Augusta Ada Byron (1816-1852): The First Programmer
(picture)

WHY LEARN PROGRAMMING

   Three years ago there were classes in computer programming (in
B.A.S.I.C.) at  Ford's Dearborn Engine Plant and at Ford UAW
Local 600. Also,there were classes in programming in B.A.S.I.C.
at many local public schools.  Now, in 1988, computer programming
classes are gone from the Ford Rouge Factory and there are fewer
or none left in local public schools. For example, there are no
longer classes in programming in B.A.S.I.C. in the Dearborn
Public Schools. Thus there has been a substantial change in com-
puter education both at the workplace and in the public schools.
Why has this change occurred? Also there were public moneys, from
both the Federal and state government allocated to provide for
these and other classes. When public funds are involved, there
are a set of regulations to be followed so there can be public
scrutiny of what is happening with the money. The money is still
being provided but the classes are gone. What has happened?

   Over the past three years UAW members at the Ford Rouge Plant
made clear that they were interested in learning computer
programming. The personal computer is a young technology. It's
only beginning to be developed in terms of uses at home. More
people have computers in their homes now and they want to be able
to use them for things they previously had done on paper. Most
software is not customized to the individual. If you have
knowledge of how to program, you can make the software meet your
needs. For example, on IBM compatible machines, you can write
BATCH programs which will help you use your application programs
more efficiently. You can use the computer even when there is no
commercial software for the task you want to accomplish. Also, it
is much harder to use store bought software when you are not
familiar with programming skills.  By learning to program you
learn how the computer works so you aren't intimidated by it. And
personal computers are being used in more and more workplaces,
which makes it doubly important to be able to get the computer to
do what it is needed to do.

   It is not only that people need to know the computer to be
able to do their job. It is also that the computer needs to be
developed in the workplace. The workers who operate computers
will need to develop the uses of the computer and will have to be
able to get them to work. Our times are like the early days of
the industrial revolution when machines were first introduced
into factories. Workers needed to know the principles of physics,
mathematics etc. to be able to get the machines to function
properly. But the factory owners were afraid of educated workers.
They wanted workers who were obedient and passive and resigned to
their conditions. Thus, it became necessary to set up special
technical schools for workers called Mechanics Institutes to make
this technical knowledge available to the workers who needed it.
And when those schools finally were set up, there was a sharp
struggle as to whether the factory owners or the workers would
determine the content of the classes offered.

   Herman Goldstine,in his book The Computer from Pascal to
vonNeumann (1972, N.J.,p 32) offers an account of the problem
workers faced being denied technical education. He writes:

   "This exclusion was going on just at the time when the Indust-
   rial Revolution was making education ever more essential for
   all members of society. In 1823 George Birkbeck (1776-1841)
   founded his first Mechanics' Institute in Scotland, and sim-
   ilar institutes spread into England under the patronage of
   Henry Brougham (1778-1868). These brought to the workingman
   the advantages of technological training just when it was most
   needed in England...for example, Stephenson, the inventor of
   the locomotive, was a poor boy who taught himself to read when
   he was seventeen." (p 32)

   A similar but more subtle exclusion from technical education
has occurred at the Ford Rouge Plant. Company and union officials
say that the union is teaching computers at UAW Local 600.
Originally there were computer programming classes at Local 600.
They were taught by teachers from Henry Ford Community College.
But suddenly those classes were ended, and a private subcon-
tractor was brought in to teach computers. The new classes,
however, were no longer classes in computer programming. They
were classes in how to use a certain word processing program, or
how to use a particular spreadsheet program. Why were the
computer programming classes designed by Henry Ford Community
College teachers ended at the union local? Why were computer
applications substituted for computer programming in the classes
at the union hall? And why were these classes then used to cut
out classes in computer programming at the Dearborn Engine Plant?
The computer programming classes at the Dearborn Engine Plant
were part of the pilot program set up in early 1984 under the
UAW-Ford contract. Computer literacy classes which included 60
hours of computer programming instruction were made available as
part of this pilot program. The classes were supposed to be
available as adult education classes run by a local school
system. And there was State and federal funding supplementing the
class offerings. A Professor from the University of Michigan who
wrote an evaluation of the program in Spring, 1984 said that the
computer classes were the most important aspect of the program,
and he recommended that whenever other programs be set up, they
include computer offerings. His evaluation was used to justify
further funding from the State of Michigan and the federal
government. These funds required all workers at the Rouge be
notified of all the classes that are offered at the Dearborn En-
gine Plant. Yet in Fall 1985, the computer classes were removed
from the brochures advertising the classes available at the
Dearborn Engine Plant. And then some of the computer classes were
cut out. When UAW members tried to inquire about why this was
happening, they were told that there were computer classes at the
union hall.

   But the federal funding required that the contract signed by
Ford and the UAW to provide computer literacy classes at the
D.E.P. be maintained. And the D.E.P contract also stipulated that
there would be advanced computer classes offered at the D.E.P.
Yet when U.A.W. members tried to register for these advanced
classes, they were told that they wouldn't be available. They
were told they could take classes in computer applications at
Local 600. Why was such an effort made, despite federal funding
requirements, to cut out the computer programming classes at the
Dearborn Engine Plant?

  First of all, it is said that workers won't have to program a
computer, they will only have to operate it. Thus computer
classes need only teach how to run a commercial computer program.
But the computer is not a word processor or a spreadsheet or a
data base machine. Almost any personal computer can be used in a
variety of ways. It can be used for word processing, to run a
C.A.D. program, to run a spreadsheet. It can be used to run
programmable controllers or robots. It can be used to do type-
setting. To learn a particular application is not necessarily
helpful in learning the flexible nature of the computer. The per-
sonal computer is an all purpose machine. It has only begun to
show its varied potential. But to utilize this machine, you have
to understand how it works and how to get it to do what you want.
Thus you need to know how to program it.

   In the 1930's, some auto workers in Flint, MI, had lathes in
their basements so they could become familiar with the operation
of the machine and be able, therefore, to get it to do what was
needed at work. The same goes for the personal computer. The more
you use it at home, the more you will be able to understand how
it functions and be able to use it at work. Some U.S. cor-
porations seem to believe they can control the computer, so they
are keeping workers and schoolchildren from learning programming.
One company official told a mother his company didn't want
people learning programming. They would teach "whatever someone
needed to know." Also supervisors have said they don't want
workers typing in programs, tying up the machines. And maybe it
is feared that if workers learn to program they will change the
operation of a machine.  But are these fears realistic?

   First of all, the computer is new. People using computers will
be running into new, unknown situations. Management may claim
they don't want workers trying to deal with these situations,
they want PRODUCTION. There are salary programmers. But they
can't write everything that needs to be written, because they
can't do all the customizing that is needed. To get production,
management will need workers on the shop floor who are able to
solve the day to day problems that occur in the course of oper-
ating the computers that are being installed on the shop floor.

   In the precomputer era, someone running a keller machine
needed to know what cutters to use in what areas, what direction
to run in, how to set the job up, the size of the tracers, which
cutters to use for certain areas, when the job was done, etc. It
was only practical experience that made it possible to run the
machines. Then numerical control machines were introduced, which
run with paper tapes as programs. They are often programmed by
salary programmers at another location. But if the cutter gets
dull or hits screws and dowels, or there's too much stock or no
stock (for example you can't take off 4 inches of stock at one
time as you would break the cutter) then the operator has to
intervene in the program and override it. Kellers, now, are run
by direct numerical control, rather than by paper tape. In the
past, there wasn't enough memory storage to store the whole pro-
gram at one time to be able to edit it. Now the operator can load
the whole program into the machine at one time to run it. If the
operator finds something is wrong, it is now possible to edit the
program and correct the error. Therefore, it increases efficiency
for the operator to know how to program the machine. Also, it is
more likely a worker who doesn't know programming will make some
mistake that may interfere with the program in a machine, while
one who knows how the machine operates and is equipped to solve
its problems may actually improve the situation. It is workers
who keep machines running, and they need certain necessary
knowledge to be able to do their work.

   Learning B.A.S.I.C. can be an easy introduction to how a
computer uses programs. It also makes it possible for a beginner
to write simple programs. If someone doesn't know B.A.S.I.C. or
another programming language, he doesn't know what a computer can
do. He doesn't know if the computer is capable of adding,
subtracting, or how it does it. If he runs into trouble, he has
no idea why. Someone who has learned a little B.A.S.I.C.
programming, however, knows that a computer can do calculations.
He understands how a program can get stuck in a loop. And if he
needs to go for some kind of specific training, for example, for
robotics training, or numerical control training, he has a back-
ground that helps because he already knows what a program is. He
might be learning another programming language, but he doesn't
have to start from scratch.

   Learning to program a computer can also help to demystify the
computer. It can give someone confidence in using the computer
because the person knows he can control the outcome by changing a
few commands. Also, he has accurate knowledge of how the computer
functions. Thus he can deal with the unexpected and the problems.
One of the pioneers in the development of the personal computer,
David Ahl, observed that there is great misunderstanding about
the kind training required to develop the technology of the
personal computer. "We are dealing with one of the most important
concepts and tools developed by man," he says,"and yet some
continue to hope they can check it off as they do driver
education or typing." (Creative Computing, Nov. 1984, p.16)

The Cover of Personal Computing
announced the Pet Computer
October, 1977
(picture)

COMMODORE TIPS & TRICKS

BLOCKS FREE
   If you would like to know how many blocks are free on a disk,
enter: LOAD"$$",8. Then LIST. The result will display the blocks
free on the disk but not the directory.

DIRECT MODE DISK-ERROR READER
   The next time you need to read the disk error channel, try
this line in direct mode:

  OPEN 1,8,15 : POKE 58,0 : {about 20    spaces} INPUT#1,A$,B$,C$,D$ :
  PRINT A$,B$,C$,D$ : CLOSE 1

EASY LOAD AND RUN
   Type: LOAD"filename",8 {shift and run-stop keys}

SHORT FILE-READER
   Here is a one-liner for reading sequential files. Change
   "filename" to the name of your sequential file and type in RUN:

  2 OPEN8,8,8"filename" : FOR I = 0 TO 1 : GET#8,A$ : I = ST :
    PRINT A$;:     NEXT : CLOSE 8 : END

NEW FIRST FILE
   This program will let you swap the first file in a directory with
any other file on the disk:

  10 INPUT" name of current first program "; F$
  20 INPUT" name of program to be first "; P$
  30 PRINT" validating disk " : OPEN 15,8,15,"V0"
  40 PRINT" swapping files " : F1$ = F$+"." : P1$ = P$+"."
  50 PRINT#15,"C0:"+F1$+"=0:"+F$ : PRINT#15,"S0:"+F$
  60 PRINT#15,"C0:"+P1$+"=0:"+P$ : PRINT#15,"S0:"+P$
  70 PRINT#15,"R0:"+P$+"=0:"+P1$ : PRINT#15,"R0:"+F$+"=0:"+F1$
  80 CLOSE15 : PRINT"{ 2 curser downs}      all done!"

    STAFF
Steve Alexander
Ronda Hauben
Bill Rohler
Norman O. Thompson

The "Amateur Computerist" invites contributions of articles,
programs, etc. Send submissions to:
       R. Hauben
       P.O. Box 4344
       Dearborn, MI 48126

